# -*- coding: utf-8 -*-
from openerp.osv import orm


class website(orm.Model):
    _inherit = 'website'
 
    def sale_product_domain(self, cr, uid, ids, context=None):
        domain = super(website, self).sale_product_domain(cr, uid, ids, context=context)
        domain += [('list_price', '>', 0)]
        # domain += [('virtual_available', '>', 0)]
        return domain

    
