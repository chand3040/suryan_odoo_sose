# -*- coding: utf-8 -*-

{
    'name': 'Website sale Domain',
    'version': '1.0',
    'category': 'Ecommerce',
    'description': """
This module added domain in website product page
""",
    'author': 'DRC Systems',
    'depends': ['website_sale'],
    'installable': True,
}
