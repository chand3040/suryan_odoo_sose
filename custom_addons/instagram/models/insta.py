# -*- coding: utf-8 -*-

from openerp.osv import osv
from openerp import models, fields, api

from urllib2 import urlopen, Request, HTTPError

import json
import werkzeug

class InstagramConfigSetting(osv.osv_memory):
    _inherit = 'base.config.settings'
    
    instagram_client_id = fields.Char(default=lambda self: self.env['ir.config_parameter'].get_param('instagram_client_id')) # 1690645049
    instagram_secret_id = fields.Char()
    instagram_user_id = fields.Char()
    instagram_access_token = fields.Char(default=lambda self: self.env['ir.config_parameter'].get_param('instagram_access_token')) #1690645049.f324c45.ed308d1e6ecc41f39b303b6611600380

    @api.multi
    def set_instagram_param(self):
        ir_config_param = self.env['ir.config_parameter']
        for config in self:
            config = config
            instagram_client_id = config.instagram_client_id
            instagram_access_token = config.instagram_access_token

            if instagram_client_id:
                ir_config_param.set_param('instagram_client_id', instagram_client_id, groups=['base.group_system'])
            if instagram_access_token:
                ir_config_param.set_param('instagram_access_token', instagram_access_token, groups=['base.group_system'])
