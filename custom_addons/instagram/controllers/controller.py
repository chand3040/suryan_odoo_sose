# -*- coding: utf-8 -*-

# import werkzeug
from openerp import http
from openerp.http import request
from openerp.addons.website.controllers.main import Website as Home


from urllib2 import urlopen, Request, HTTPError

import json
import logging
import werkzeug

MAX_INSTAGRAM_IMAGE = 16

_logger = logging.getLogger(__name__)

class Website(Home):

    def _request(self, url, params=None):
        if params:
            params = werkzeug.url_encode(params)
            url = url + '?' + params
        try:
            request_url = Request(url)
            return json.load(urlopen(request_url, timeout=10))
        except HTTPError, e:
            _logger.debug("Issue in Authorization")
            raise


    @http.route()
    def index(self, *args, **kw):
        response = super(Website, self).index(*args, **kw)
        insta_url = []
        ir_config_param = request.env['ir.config_parameter'].sudo()
        instagram_client_id = ir_config_param.get_param('instagram_client_id')
        instagram_access_token = ir_config_param.get_param('instagram_access_token')
        if instagram_client_id and instagram_access_token:
            # param = {'access_token': instagram_access_token, 'count': MAX_INSTAGRAM_IMAGE}
            param = {'access_token': instagram_access_token, 'count': MAX_INSTAGRAM_IMAGE}
            # param = {'access_token': instagram_access_token}
            url = 'https://api.instagram.com/v1/users/%s/media/recent' % instagram_client_id

            # https://api.instagram.com/v1/users/%s/media/recent/?access_token=1690645049.f324c45.ed308d1e6ecc41f39b303b6611600380

            data = self._request(url, param)

            for data in data['data']:
                insta = {}
                insta['like'] = data['likes']['count']
                insta['img'] = data['images']['standard_resolution']['url']
                insta['link'] = data['link']
                insta_url.append(insta)
                # insta_url.append(data['images']['standard_resolution']['url'])
            response.qcontext['insta_info'] = insta_url

        return response
