# -*- coding: utf-8 -*-

{
    'name': 'Instagram',
    'version': '1.0',
    'category': 'Website',
    'description': """
This module integrate Instagram with odoo
""",
    'author': 'DRC Systems',
    'depends': ['ecommerce_theme'],
    'data': [
        'views/user_views.xml',
        # 'views/blog_views.xml',
        'views/snippets.xml',
    ],
    'installable': True,
}
