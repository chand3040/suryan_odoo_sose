function pos_product_available(instance, module){

    _t = instance.web._t;

    var PosModelSuper = module.PosModel
    module.PosModel = module.PosModel.extend({
        load_server_data: function(){
            var self = this;
            var loaded = PosModelSuper.prototype.load_server_data.call(this);

            loaded = loaded.then(function(){
                return self.fetch(
                    'product.product',
                    ['qty_available'],
                    [['sale_ok','=',true],['available_in_pos','=',true]],
                    {'location': self.config.stock_location_id[0]}
                );

            }).then(function(products){
                $.each(products, function(){
                    $.extend(self.db.get_product_by_id(this.id) || {}, this)
                });
                return $.when()
            })
            return loaded;
        },
        refresh_qty_available:function(product){
            var $elem = $("[data-product-id='"+product.id+"'] .qty-tag");
            $elem.html(product.qty_available)
            if (product.qty_available <= 0 && !$elem.hasClass('not-available')){
                $elem.addClass('not-available')
            }
        },
        push_order: function(order){
            var self = this;
            var pushed = PosModelSuper.prototype.push_order.call(this, order);
            if (order){
                order.get('orderLines').each(function(line){
                    var product = line.get_product();
                    product.qty_available -= line.get_quantity();
                    self.refresh_qty_available(product);
                })
            }
            return pushed;
        },
        push_and_invoice_order: function(order){
            var self = this;
            var invoiced = PosModelSuper.prototype.push_and_invoice_order.call(this, order);

            if (order && order.get_client()){
                if (order.get("orderLines")){
                    order.get("orderLines").each(function(line){
                        var product = line.get_product();
                        product.qty_available -= line.get_quantity();
                        self.refresh_qty_available(product);
                    })
                } else if (order.orderlines){
                    order.orderlines.each(function(line){
                        var product = line.get_product();
                        product.qty_available -= line.get_quantity();
                        self.refresh_qty_available(product);
                    })
                }
            }

            return invoiced;
        },
    })

    var PosOrderlineSuper = module.Orderline
    module.Orderline = module.Orderline.extend({
        template: 'Orderline',
        set_quantity: function(quantity) {
            var self = this;
            wk_avail_pro = 0;
            wk_pro_order_line = (this.pos.get('selectedOrder')).getSelectedLine().get_quantity();
            if(!self.pos.config.wk_continous_sale) {
                var product_id = this.pos.get('selectedOrder').getSelectedLine().product.id
                wk_current_qty = parseInt($("span.product[data-product-id=" + product_id + "]").find('.qty-tag').html())
                if (quantity == '' || quantity == 'remove') {
                    wk_avail_pro = wk_current_qty + wk_pro_order_line
                } else {
                    wk_avail_pro = wk_current_qty + wk_pro_order_line - quantity;
                }
                if (wk_avail_pro < self.pos.config.wk_deny_val && (!(quantity == '' || quantity == 'remove'))) {
                    this.pos.pos_widget.screen_selector.show_popup('error', {
                        'message': _t("Warning !!!!"),
                        'comment': _t("("+(this.pos.get('selectedOrder')).getSelectedLine().product.display_name+") - "+self.pos.config.wk_error_msg+"."),
                    });
                } else {
                    PosOrderlineSuper.prototype.set_quantity.call(this, quantity);
                }
            } else {
                PosOrderlineSuper.prototype.set_quantity.call(this, quantity);
            }
            if (this.pos.get('selectedOrder').getSelectedLine()) {
                var product_id = this.pos.get('selectedOrder').getSelectedLine().product.id
                $("span.product[data-product-id=" + product_id + "]").find('.qty-tag').html(wk_avail_pro);

                if (parseInt($("span.product[data-product-id=" + product_id + "]").find('.qty-tag').html()) <= 0) {
                    $("span.product[data-product-id=" + product_id + "]").find('.qty-tag').css('background', '#d882ac');
                }
                else {
                    $("span.product[data-product-id=" + product_id + "]").find('.qty-tag').css('background', '#7f82ac');
                }
            }
        },
    });

    var PosOrderSuper = module.Order
    module.Order =module.Order.extend({
        template:'Order',
        refresh_qty_realtime:function(product){
            var order_line=this.get('orderLines');
            var $elem = $("[data-product-id='"+product.id+"'] .qty-tag");
            for(j=0;j<order_line.length;j++)
            {
                if (product.id == order_line.models[j].product.id) {
                    $elem.html(parseInt(product.qty_available) - parseInt(order_line.models[j].quantity));
                }   
            }
        },
        addProduct: function(product, options){
            var self = this;
            if(!self.pos.config.wk_continous_sale)
            {
                if(parseInt($("span.product[data-product-id=" + product.id + "]").find('.qty-tag').html()) <= self.pos.config.wk_deny_val){
                   
                    self.pos.pos_widget.screen_selector.show_popup('error',{
                                'message': _t("Warning !!!!"),
                                'comment': _t("("+product.display_name+") - "+self.pos.config.wk_error_msg+"."),
                            });
                }
                else{
                    PosOrderSuper.prototype.addProduct.call(this,product, options);
                    self.refresh_qty_realtime(product);
                }
            }
            else {
                PosOrderSuper.prototype.addProduct.call(this,product, options);
                self.refresh_qty_realtime(product);
            }

            if (parseInt($("span.product[data-product-id=" + product.id + "]").find('.qty-tag').html()) <= 0) {
                $("span.product[data-product-id=" + product.id + "]").find('.qty-tag').css('background', '#d882ac');
            }
            else {
                $("span.product[data-product-id=" + product.id + "]").find('.qty-tag').css('background', '#7f82ac');
            }
        },
        
    });

}

(function(){
    var _super = window.openerp.point_of_sale;
    window.openerp.point_of_sale = function(instance){
        _super(instance);
        var module = instance.point_of_sale;

        pos_product_available(instance, module);

        $('<link rel="stylesheet" href="/pos_product_available/static/src/css/pos.css"/>').appendTo($("head"))
    }
})()
