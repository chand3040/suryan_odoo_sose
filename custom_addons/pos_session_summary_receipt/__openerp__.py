# -*- encoding: utf-8 -*-

{
    "name": "POS Session Summary Receipt",
    "version": "1.0",
    "author": "Drc Systems",
    "category": "Point Of Sale",
    "depends": [
        "point_of_sale",
    ],
    "data": [
        'views/report_sessionsummary_header.xml',
        'views/report_sessionsummary.xml',
        'views/point_of_sale_report.xml',
    ],
    "installable": True,
}
