
from openerp import http
from openerp.http import request
from openerp.addons.website.controllers.main import Website as Home
from openerp.addons.website_sale.controllers.main import QueryURL
from openerp.addons.website_sale.controllers import main as website_sale


website_sale.PPG = 12  # Products Per Page
website_sale.PPR = 2  # Products Per Row

class Website(Home):

    @http.route()
    def index(self, *args, **kw):
        cr, uid, context = request.cr, request.uid, request.context
        response = super(Website, self).index(*args, **kw)
        keep = QueryURL('/shop', category=None, search='', attrib=[])
        category = request.env['product.public.category']
        category_obj = category.search([('parent_id', '=', False)])
        featured_product = request.env['product.template']
        featured_product_obj = featured_product.search([('is_featured', '=', True)])
        new_product_obj = featured_product.sudo().search(['&', ('website_published', '=', True), ('is_new', '=', True)], limit=3)
        # new_product_obj = featured_product.sudo().search([('website_published', '=', True)],order="create_date desc", limit=3)
        response.qcontext['public_category'] = category_obj
        response.qcontext['featured_product'] = featured_product_obj
        response.qcontext['new_product'] = new_product_obj
        response.qcontext['keep'] = keep
        response.qcontext['page'] = 'homepage'

        return response

    @http.route('/page/<page:page>', type='http', auth="public", website=True)
    def page(self, page, **opt):
        response = super(Website, self).page(page, **opt)
        keep = QueryURL('/shop', category=None, search='', attrib=[])
        response.qcontext['keep'] = keep

        return response

    @http.route('/contactus', type='http', auth="public", website=True)
    def contactus(self, **opt):
        keep = QueryURL('/shop', category=None, search='', attrib=[])
        values = {'keep': keep}
        return request.render('ecommerce_theme.contactus', values)

    @http.route('/carrers', type='http', auth="public", website=True)
    def carrers(self, **opt):
        keep = QueryURL('/shop', category=None, search='', attrib=[])
        values = {'keep': keep}
        return request.render('ecommerce_theme.carrers', values)

    @http.route('/shipping_policy', type='http', auth="public", website=True)
    def shipping_policy(self, **opt):
        keep = QueryURL('/shop', category=None, search='', attrib=[])
        values = {'keep': keep}
        return request.render('ecommerce_theme.shipping_policy', values)

    @http.route('/privacy_policy', type='http', auth="public", website=True)
    def privacy_policy(self, **opt):
        keep = QueryURL('/shop', category=None, search='', attrib=[])
        values = {'keep': keep}
        return request.render('ecommerce_theme.privacy_policy', values)

    @http.route('/disclaimer', type='http', auth="public", website=True)
    def disclaimer(self, **opt):
        keep = QueryURL('/shop', category=None, search='', attrib=[])
        values = {'keep': keep}
        return request.render('ecommerce_theme.disclaimer', values)

    # @http.route(['/shop/policy'], type='http', auth="public", website=True)
    # def policy(self, **kwargs):
    #     return request.website.render("ecommerce_theme.policy", {})

class WebsiteSale(website_sale.website_sale):

    @http.route(['/shop/cart'], type='http', auth="public", website=True)
    def cart(self, **post):
        cr, uid, context = request.cr, request.uid, request.context
        response = super(WebsiteSale, self).cart(**post)
        acquirer = request.registry['payment.acquirer']
        acquirer_ids = acquirer.search(cr, uid, [('website_published', '=', True)], context=context, limit=1)
        acquirer_obj = acquirer.browse(cr, uid, acquirer_ids, context=context)
        response.qcontext['acquirer'] = acquirer_obj

        return response

    @http.route()
    def product(self, product, category='', search='', **kwargs):
        response = super(WebsiteSale, self).product(product, **kwargs)
        product_obj = request.env['product.template']
        categ_ids = product.public_categ_ids
        similiar_product_obj = product_obj.search(['&', '&', ('public_categ_ids', 'in', categ_ids.ids), ('product_brand_id', 'not in', [product.product_brand_id.id]), ('id', 'not in', [product.id])], limit=4)
        other_product_obj = product_obj.search(['&', ('public_categ_ids', 'in', categ_ids.ids), ('id', 'not in', [product.id])], limit=5)
        response.qcontext['other_product'] = other_product_obj
        response.qcontext['similiar_product'] = similiar_product_obj

        return response

    def checkout_values(self, data=None):
        response =  super(WebsiteSale, self).checkout_values(data=data)
        city = request.env['res.partner.city'].sudo().search([])
        state_lst = ['GUJARAT', 'MAHARASHTRA']
        country_lst = ['India']
        states = request.env['res.country.state'].sudo().search([('name', 'in', state_lst)])
        country = request.env['res.country'].sudo().search([('name', 'in', country_lst)])
        response['city'] = city
        response['shipping_city'] = city
        response['states'] = states
        response['countries'] = country
        return response