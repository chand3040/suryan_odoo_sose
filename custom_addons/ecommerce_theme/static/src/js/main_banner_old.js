// var mainBanner = $("#mainBanner");
//         mainBanner.owlCarousel({
//         loop:true,
//         margin:0,
//         nav:false,
//         items:1,
//         autoplay: true,
//     });
//     $('.integrations-slider').owlCarousel({
//         loop:true,
//         margin:0,
//         nav:false,
//         dots:true,
//         items:1,
//     });
$('div.categorie-box a[data-toggle="collapse"]').click(function(){
    $(this).find('i').toggleClass("fa-plus-square fa-minus-square");
    console.log($(this).find('i'));
});
function setupLabel() {
    if ($('.label_check input').length) {
        $('.label_check').each(function(){ 
            $(this).removeClass('c_on');
        });
        $('.label_check input:checked').each(function(){ 
            $(this).parent('.label_check').addClass('c_on');
        });                
    };
    if ($('.label_radio input').length) {
        $('.label_radio').each(function(){ 
            $(this).removeClass('r_on');
        });
        $('.label_radio input:checked').each(function(){ 
            $(this).parent('label').addClass('r_on');
        });
    };
};

$(".small-btn").click(function(){
    $(".nav-link").slideToggle();   
});

$('.gallery-box').click( function() { 
    window.location.href= $(this).find('a').attr('href');
});
$(".small-btn").click(function(){
    $(".nav-link").slideToggle();
});

$(document).ready(function() {
    $('.label_check, .label_radio').click(function(){
        setupLabel();
    });
    setupLabel();
    $('.oe_website_sale').each(function () {
        var oe_website_sale = this;

        var $shippingDifferent = $("input[name='shipping_id']", oe_website_sale);
        $shippingDifferent.change(function (event) {
            var value = +$(this).val();
            var data = $(this).data();
            var $snipping = $(".js_shipping", oe_website_sale);
            var $inputs = $snipping.find("input");
            var $selects = $snipping.find("select");

            $snipping.toggle(!!value);
            $inputs.attr("readonly", value <= 0 ? null : "readonly" ).prop("readonly", value <= 0 ? null : "readonly" );
            $selects.attr("disabled", value <= 0 ? null : "disabled" ).prop("disabled", value <= 0 ? null : "disabled" );

            $inputs.each(function () {
                $(this).val( data[$(this).attr("name")] || "" );
            });
        });
    });

    var lang = ["en_US"];
    path = window.location.pathname.replace('/', '');
    mul_lang_exist = (lang.indexOf(path));
    if (window.location.pathname != '/' && mul_lang_exist != 0) {
        if(!$("#nav-main").hasClass("fiexd")) {
            $("#nav-main").css("position","inherit");
            $("#nav-main").css("background","#62a83d");
            $(".navbar-brand").css("background","#fff");
        }
    }
    var window_height = $(window).height()-50;
    $("#myCarousel").css ("height",window_height + "px");
     $("#myCarousel .carousel-inner").each(function () {
        var img_src = $(this).children(".item").children("img").attr('src');
        $(this).children(".item").css("background-image", 'url(' + img_src + ')');
        $(this).children(".item").children("img").hide();
    });
    $(".bottom-nav").css("top", $(window).height() - 48  + "px");
    
    
    $(".search-block a").click(function(){
        $(".search-block .search-box").slideToggle();
    });
    $(".search-box .icon").click(function(){
        $(".search-block .search-box").slideToggle();
    });
    $("#footer .bottom-arrow").click(function(){
        $("html, body").animate({ scrollTop: 0 },1000); 
    });
    /* manu fiexd js start*/
    if($(window).scrollTop() > 38)  
    {
        $("#nav-main").addClass("fiexd");
        $("#nav-main").css("position","fixed");
    }
    else
    {
        $("#nav-main").removeClass("fiexd");
        if (window.location.pathname != '/') {
            $("#nav-main").css("position","inherit");
        }
    }
    if($(".our-functionality").offset()){
        var top_value = $(".our-functionality").offset().top ; 
        top_value = top_value - 48 - $("#nav-main").height(); 
        if ($(window).scrollTop() > top_value)
        {
            $(".bottom-nav").addClass("fiexd");
            $(".bottom-nav").css("top",$("#nav-main").height() + "px");
        }
        else
        {
            $(".bottom-nav").removeClass("fiexd");
            $(".bottom-nav").css("top", $(window).height() - 48  + "px");
        }
    }
    if($(window).width() > 767)
    {
        
    }
    else
    {
        $("#nav-main").addClass("fiexd");
        $("#nav-main").css("position","fixed");
    }
    
});
$(window).resize(function() {
    // var window_height = $(window).height();
    // $("#myCarousel").css ("height",window_height + "px"); 
//     if ($(".bottom-nav").hasClass("fiexd"))
//     {
        
//     }
//     else
//     {
//         $(".bottom-nav").css("top", $(window).height() - 48  + "px");
//     }
    if($(window).width() > 767)
    {
        $("#nav-main").removeClass("fiexd");
        if (window.location.pathname != '/') {
            $("#nav-main").css("position","inherit");
        }
    }
    else
    {
        $("#nav-main").addClass("fiexd");
        $("#nav-main").css("position","fixed");
    }
});
$(window).scroll(function(){
    if($(window).width() > 767)
    {
        if($(window).scrollTop() > 38)  
        {
            $("#nav-main").addClass("fiexd");
            $("#nav-main").css("position","fixed");
        }
        else
        {
            $("#nav-main").removeClass("fiexd");
            if (window.location.pathname != '/') {
                $("#nav-main").css("position","inherit");
            }
        }
    }
    else
    {
        $("#nav-main").addClass("fiexd");
        $("#nav-main").css("position","fixed");
    }
    if($(".our-functionality").offset()){
        var top_value = $(".our-functionality").offset().top ; 
        top_value = top_value - 48 - $("#nav-main").height(); 
        if ($(window).scrollTop() > top_value)
        {
            $(".bottom-nav").addClass("fiexd");
            $(".bottom-nav").css("top",$("#nav-main").height() + "px");
        }
        else
        {
            $(".bottom-nav").removeClass("fiexd");
            $(".bottom-nav").css("top", $(window).height() - 48  + "px");
        }
    }
});
