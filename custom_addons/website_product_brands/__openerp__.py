# -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#################################################################################
{
    'name': "Website: Product Brands",
    'category': 'Website',
# "license": 'OpenERP AGPL + Private Use',
    'sequence':1,
    'summary': """It allows you to add brand to the products and manage them accordingly. """,
    'website': 'http://www.webkul.com',
    'version': '1.1',
    'description':"""
ODOO Website Product Brands
===========================
    **Help and Support**
===========================
.. |icon_features| image:: website_product_brands/static/description/icon-features.png
.. |icon_support| image:: website_product_brands/static/description/icon-support.png
.. |icon_help| image:: website_product_brands/static/description/icon-help.png

|icon_help| `Help <https://webkul.com/ticket/open.php>`_ |icon_support| `Support <https://webkul.com/ticket/open.php>`_ |icon_features| `Request new Feature(s) <https://webkul.com/ticket/open.php>`_
    """,
    'author': 'Webkul Software Pvt. Ltd.',
    'depends': ['website_sale'],
    'installable': True,
    'data': [
        'security/ir.model.access.csv',
        # 'data/data.xml',
        'views/website_product_brands.xml',
        'views/snippet.xml',
        # 'views/template.xml',
    ],    
    'application': True,
    "price": 35,
    "currency": 'EUR',

    "images":['static/description/Banner.jpg']
    
    
}