# -*- coding: utf-8 -*-
from openerp.addons.web.http import request
from openerp.osv import orm


class QWeb(orm.AbstractModel):
    """ QWeb object for rendering stuff in the website context
    """
    _inherit = 'website.qweb'

    URL_ATTRS = {
        'form': 'action',
        'a': 'href',
    }

    CDN_TRIGGERS = {
        'link':    'href',
        'script':  'src',
        'img':     'src',
    }


    def render_attribute(self, element, name, value, qwebcontext):
        context = qwebcontext.context or {}
        if not context.get('rendering_bundle'):
            if name == self.URL_ATTRS.get(element.tag) and qwebcontext.get('url_for'):
                value = qwebcontext.get('url_for')(value)
            elif request and request.website and request.website.cdn_activated and (name == self.URL_ATTRS.get(element.tag) or name == self.CDN_TRIGGERS.get(element.tag)):
                value = request.website.get_cdn_url(value, request)
        return super(QWeb, self).render_attribute(element, name, value, qwebcontext)
