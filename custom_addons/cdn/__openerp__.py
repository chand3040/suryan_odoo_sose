# -*- coding: utf-8 -*-
{
    'name': "CDN ODOO 8",

    'summary': """
        Allows to setup the CDN server to load the assets faster.""",

    'description': """
        Can use any CDN server.
        You just need to configure the CDN base URL.
    """,

    'author': "DRC Systems",
    'website': "http://www.drcsystems.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'website'],

    # always loaded
    'data': [
        'cdn_view.xml',
    ],

    # only loaded in demonstration mode
    'demo': [],
}