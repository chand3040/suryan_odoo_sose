# -*- coding: utf-8 -*-

{
    'name': 'Website sale Product Stock Available',
    'version': '1.0',
    'category': 'Ecommerce',
    'description': """
This module dispaly product stock in website product page
""",
    'author': 'DRC Systems',
    'depends': ['website_sale', 'stock'],
    'data': [
        'views/res_config_view.xml',
        'views/website_templates.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
}
