# -*- encoding: utf-8 -*-

{
    "name": "POS Rounding",
    "version": "1.0",
    "author": "Drc Systems",
    "category": "Point Of Sale",
    "depends": [
        "point_of_sale",
    ],
    "data": [
        "views/pos_views.xml",
        "views/pos.xml",
    ],
    "installable": True,
}
