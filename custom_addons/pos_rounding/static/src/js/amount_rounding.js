openerp.pos_rounding = function (instance) {
    var module = instance.point_of_sale;

    module.Order = module.Order.extend({
        getTotalTaxIncluded: function() {
            var total = module.Order.__super__.getTotalTaxIncluded.apply(this, arguments);
            return Math.round(total);
        },
    });


    module.OrderWidget = module.OrderWidget.extend({
        update_summary: function() {
            this._super.apply(this, arguments);
            var order = this.pos.get('selectedOrder');
            var real_total =  (order.get('orderLines')).reduce((function(sum, orderLine) {
                return sum + orderLine.get_price_with_tax();
            }), 0);
            var total     = order ? order.getTotalTaxIncluded() : 0;
            var diff = total - real_total;
            var taxes     = order ? total - order.getTotalTaxExcluded() - diff : 0;

            this.el.querySelector('.summary .total .subentry .value').textContent = this.format_currency(taxes);
        },
    });

};
