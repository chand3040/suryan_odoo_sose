# -*- coding: utf-8 -*-

from openerp import models, fields, api
import openerp.addons.decimal_precision as dp


class PosOrder(models.Model):
    _inherit = 'pos.order'

    @api.multi
    def _compute_pos_order_actual_amount(self):
        cur = self.env['res.currency']
        for pos_order in self:
            cur = pos_order.pricelist_id.currency_id
            val1 = 0
            for line in pos_order.lines:
                val1 += line.price_subtotal_incl
            pos_order.pos_order_actual_amount = cur.round(val1)

    pos_order_actual_amount = fields.Float(compute='_compute_pos_order_actual_amount', digits_compute=dp.get_precision('Account'), string='Total Amount(Without Rounding)')


    @api.multi
    def write(self, vals):
        res = super(PosOrder, self).write(vals)
        round_diff = self.env['pos.round.diff']
        for pos_order in self:
            diff_amount = 0
            diff_amount += pos_order.amount_total - pos_order.pos_order_actual_amount
            order_exist = round_diff.search([('pos_order_id', '=', pos_order.id)], limit=1)
            if order_exist:
                order_exist.write({'diff_amount': diff_amount})
            else:
                round_diff.create({'pos_order_id': pos_order.id, 'diff_amount': diff_amount})
        return res


class PosConfig(models.Model):
    _inherit = 'pos.config'

    @api.multi
    def _compute_pos_loss_gain_diff(self):
        self.pos_loss_gain_diff = sum(round_diff.diff_amount for round_diff in self.env['pos.round.diff'].search([]))


    pos_loss_gain_diff = fields.Float(compute='_compute_pos_loss_gain_diff', digits_compute=dp.get_precision('Account'), string='Gain/Loss')




class pos_rounding_diff(models.Model):
    _name = 'pos.round.diff'

    pos_order_id = fields.Many2one('pos.order', 'POS Order')
    diff_amount = fields.Float(digits_compute=dp.get_precision('Account'), string='Gain/loss in POS Rounding')
