from openerp import models, fields, api
from openerp import tools

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from PIL import Image
from openerp.exceptions import Warning


class ProductImage(models.Model):
    _name = 'product.image'

    @api.multi
    def _compute_image(self):
        for product_image in self:
            img_data = tools.image_get_resized_images(product_image.image, avoid_resize_medium=True)
            product_image.image_medium = img_data['image_medium']
            product_image.image_small = img_data['image_small']

    name = fields.Char(readonly=True)
    description = fields.Text()
    image = fields.Binary(help="This field holds the image used as image for the product, limited to 1024x1024px.")
    image_medium = fields.Binary(compute="_compute_image",
        string="Medium-sized image")
    image_small = fields.Binary(compute="_compute_image",
        string="Small-sized image")
    product_tmpl_id = fields.Many2one('product.template', 'Product')

    @api.onchange('image')
    def _onchange_image(self):
        if self.image:
            image_minimum_width = 630
            image_minimum_height = 400
            base64_source = tools.image_resize_image_big(self.image)
            image_stream = StringIO.StringIO(base64_source.decode('base64'))
            image = Image.open(image_stream)
            size = image.size
            ratio = round(size[0] / float(size[1]),2)
            if 1.5 > ratio or ratio >= 1.595:
                raise Warning('Maintain approximate ratio for other Product images:- 1.5:1')
            if size[0] < image_minimum_width or size[1] < image_minimum_height:
                raise Warning('Minimum Image Size for other Product images: %s*%s' % (image_minimum_width, image_minimum_height))
    
    # @api.model
    # def create(self, vals):
    #     if vals.get('image'):
            # image_minimum_width = 630
            # image_minimum_height = 400
            # base64_source = tools.image_resize_image_big(vals.get('image'))
            # image_stream = StringIO.StringIO(base64_source.decode('base64'))
            # image = Image.open(image_stream)
            # size = image.size
            # ratio = round(size[0] / float(size[1]),2)
            # if 1.5 > ratio or ratio >= 1.595:
            #     raise Warning('Maintain approximate ratio for other Product images:- 1.5:1')
            # if size[0] < image_minimum_width or size[1] < image_minimum_height:
            #     raise Warning('Minimum Image Size for other Product images: %s*%s' % (image_minimum_width, image_minimum_height))
    #     return super(ProductImage, self).create(vals)

class ProductProduct(models.Model):
    _inherit = 'product.product'

    images = fields.One2many(related='product_tmpl_id.images',
                                 relation="product.image",
                                 store=False)


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    images = fields.One2many('product.image', 'product_tmpl_id')

    @api.multi
    def write(self, vals):
        res = super(ProductTemplate, self).write(vals)
        ProductImage = self.env['product.image']
        product_image = ProductImage.search(['&', ('product_tmpl_id', '=', self.id), ('name', '=', 'main')])
        if product_image:
            product_image.image = self.image
        else:
            product_img_data = {'name': 'main','description': 'Main', 'image': self.image, 'product_tmpl_id': self.id}
            ProductImage.create(product_img_data)
        return res

